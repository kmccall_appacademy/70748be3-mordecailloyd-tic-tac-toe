class Board
  def initialize(initialgrid=[[nil,nil,nil],[nil,nil,nil],[nil,nil,nil]])
    @grid=initialgrid
  end

  def grid
    @grid
  end

  def place_mark(location,symbol)
    @grid[location[0]][location[1]]=symbol
  end

  def empty?(pos)
    @grid[pos[0]][pos[1]] == nil
  end

  def winner
    if winner_symbol(:X)
      return :X
    end
    if winner_symbol(:O)
      return :O
    end
    return nil
  end

  def over?
    if winner != nil
      return true
    end
    if full?
      return true
    end
    false
  end

  def full?
    @grid[0].all? do |value|
      value != nil
    end && @grid[1].all? do |value|
      value != nil
    end && @grid[2].all? do |value|
      value != nil
    end
  end


  def winner_symbol(symbol)
    if @grid[0]==[symbol]*3
      return true
    end
    if @grid[1]==[symbol]*3
      return true
    end
    if @grid[2]==[symbol]*3
      return true
    end
    if @grid[1][1] == symbol
      if @grid [0][2] && @grid[2][0] == symbol
        return true
      end
      if @grid [0][0] && @grid[2][2] == symbol
        return true
      end
    end
    column0=@grid.map do |row|
      row[0]
    end
    if column0 == [symbol] * 3
      return true
    end
    column1=@grid.map do |row|
      row[1]
    end
    if column1 == [symbol] * 3
      return true
    end
    column2=@grid.map do |row|
      row[2]
    end
    if column2 == [symbol] * 3
      return true
    end
  false
  end
end
