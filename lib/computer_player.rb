class ComputerPlayer
  attr_accessor :mark


  def initialize(name)
    @name=name
    @board=[]

  end
  def display(board)
    @board=board
  end
  def board
    @board
  end
  def get_move
    (0..2).each do |idx1|
      (0..2).each do |idx2|
        if @board.empty?([idx1,idx2])
          duplicate=Marshal.load(Marshal.dump(@board))
          duplicate.place_mark([idx1,idx2],@mark)
          if duplicate.winner
            return [idx1,idx2]
          end
        end
      end
    end
    while true
      pos1=rand(0..2)
      pos2=rand(0..2)
      if @board.empty?([pos1,pos2])
        return [pos1,pos2]
      end
    end
  end


end
