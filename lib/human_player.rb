class HumanPlayer
  def initialize(name)
    @name = name
  end
  def name
    @name
  end

  def get_move
    puts "Where would you like to place?"
    pos=gets
    pos=pos.split(", ")
    pos=pos.map {|value| value.to_i }
  end

  def display(board)
    print board.grid

  end
end
