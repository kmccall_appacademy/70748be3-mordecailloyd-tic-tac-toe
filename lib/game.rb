require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game
  def initialize(p1,p2)
    @player1=p1
    @player2=p2
    @board=Board.new
    @currentplayer=@player1
  end
  def board
    @board
  end

  def play_turn
    move=@currentplayer.get_move
    @board.place_mark(move,@currentplayer.mark)
    switch_players!
  end

  def switch_players!
    if @currentplayer == @player1
      @currentplayer = @player2
    else
      @currentplayer = @player1
    end
  end

  def current_player
    @currentplayer
  end

end
